/* bug-buddy bug submitting program
 *
 * Copyright (C) 2006 Fernando Herrera
 *
 * Author:  Fernando Herrera  <fherrera@onirica.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "proccess.h"

#include <glib.h>

#include <glibtop/procmem.h>
#include <glibtop/proctime.h>


char*
proccess_get_mem_state (pid_t pid)
{
	glibtop_proc_mem proc_mem;

	glibtop_get_proc_mem (&proc_mem, pid);

	return g_strdup_printf ("Memory status: size: %" G_GUINT64_FORMAT
				" vsize: %" G_GUINT64_FORMAT
				" resident: %" G_GUINT64_FORMAT
				" share: %" G_GUINT64_FORMAT
				" rss: %" G_GUINT64_FORMAT
				" rss_rlim: %" G_GUINT64_FORMAT,
			  	proc_mem.size, proc_mem.vsize, proc_mem.resident,
			  	proc_mem.share, proc_mem.rss, proc_mem.rss_rlim);
}

char*
proccess_get_time (pid_t pid)
{
	glibtop_proc_time proc_time;

	glibtop_get_proc_time (&proc_time, pid);

	return g_strdup_printf ("CPU usage: start_time: %" G_GUINT64_FORMAT
				" rtime: %" G_GUINT64_FORMAT
				" utime: %" G_GUINT64_FORMAT
				" stime: %" G_GUINT64_FORMAT
				" cutime:%" G_GUINT64_FORMAT
				" cstime: %" G_GUINT64_FORMAT
				" timeout: %" G_GUINT64_FORMAT
			 	" it_real_value: %" G_GUINT64_FORMAT
				" frequency: %" G_GUINT64_FORMAT,
			 	proc_time.start_time, proc_time.rtime, proc_time.utime, proc_time.stime,
             	         	proc_time.cutime, proc_time.cstime, proc_time.timeout, proc_time.it_real_value,
             		 	proc_time.frequency);
}



