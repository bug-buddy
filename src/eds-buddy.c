/* bug-buddy bug submitting program
 *
 * Copyright (C) 2006 Fernando Herrera
 *
 * Author:  Fernando Herrera  <fherrera@onirica.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <config.h>
 
#include <glib.h>
#include <gconf/gconf-client.h>
 
#ifdef ENABLE_EDS
#include <libebook/e-book.h>
#endif

#include "eds-buddy.h"

char*
get_default_user_email (void)
{
	GConfClient *conf_client;
	char *tmp;
#ifdef ENABLE_EDS
	EBook *book;
	EContact *me = NULL;
#endif

	conf_client = gconf_client_get_default ();
	tmp = gconf_client_get_string (conf_client, "/apps/bug-buddy/email_address", NULL);
	g_object_unref (conf_client);

	if (tmp != NULL && strlen (tmp) > 0)
		return tmp;

#ifdef ENABLE_EDS	
	e_book_get_self (&me, &book, NULL);
	if (me == NULL)
		return NULL;

	tmp = e_contact_get (me, E_CONTACT_EMAIL_2);
	if (tmp != NULL && strlen (tmp) > 0)
		return tmp;

	tmp = e_contact_get (me, E_CONTACT_EMAIL_1);
	if (tmp != NULL && strlen (tmp) > 0)
		return tmp;
#endif

	return NULL;
}


