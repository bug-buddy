/* bug-buddy bug submitting program
 *
 * Copyright (C) 2008 - 2010 Fernando Herrera <fherrera@onirica.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <libelf.h>
#include <gelf.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>

#include <config.h>

#define N_TRIES 3

static gboolean 
find_in_debug_path (const char *filename, const char *debug_filename)
{
	char *dir;
	char *tries[N_TRIES];
	gboolean result = FALSE;
	int i;

	dir = g_path_get_dirname (filename);
	tries[0] = g_build_filename (dir, debug_filename, NULL);
	tries[1] = g_build_filename (dir, ".debug", debug_filename, NULL);
	tries[2] = g_build_filename ("/usr", "lib", "debug", dir, debug_filename, NULL);

	g_free (dir);

	for (i = 0; i < N_TRIES; ++i) {
		if (g_file_test (tries[i], G_FILE_TEST_EXISTS)) {
			result = TRUE;
			break;
		}
	}

	for (i = 0; i < N_TRIES; i++)
		g_free (tries [i]);

	return result;
}


gboolean
elf_has_debug_symbols (gint pid)
{
	gchar *proc_path;
	gchar *filename;
	int fd;
	Elf *elf;
	GElf_Ehdr elf_header;
	Elf_Scn *section = 0;

	if (pid <= 0)
		return FALSE;

	if (elf_version (EV_CURRENT) == EV_NONE ) {
		fprintf(stderr, "Elf library out of date!n");
		return FALSE;
	}

	proc_path = g_strdup_printf ("/proc/%d/exe", pid);
	filename = g_file_read_link (proc_path, NULL);
	g_free (proc_path);
	if (!filename)
		return FALSE;

	fd = open (filename, O_RDONLY);

	if ((elf = elf_begin (fd, ELF_C_READ, NULL)) == NULL){
		close (fd);
		return FALSE;
	}
	gelf_getehdr (elf, &elf_header);
	while ((section = elf_nextscn (elf, section)) != 0) {
		GElf_Shdr shdr;

		/* Check for stabs debug information */
		if (gelf_getshdr (section, &shdr) != 0) {
			if (shdr.sh_type == SHT_SYMTAB) {
				elf_end (elf);
				return TRUE;
			}
		}

		/* Check for .gnu_debuglink separete debug file */
		if (shdr.sh_type == SHT_PROGBITS) {
			char *name = elf_strptr(elf, elf_header.e_shstrndx, shdr.sh_name);
			if (strcmp (name, ".gnu_debuglink") == 0) {
				Elf_Data *edata;

				edata = elf_getdata(section, NULL);
				if (edata != NULL && find_in_debug_path (filename, (const char*) edata->d_buf)) {
					elf_end (elf);
					return TRUE;
				}
			}
		}
	}

	/* no symtab neither debug file present */
	elf_end (elf);
	return FALSE;
}
