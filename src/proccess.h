/* 
 * Author:  Brent Smith  <gnome@nextreality.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __PROCCESS_H__
#define __PROCCESS_H__

#include <glibtop/procmem.h>
#include <glibtop/proctime.h>

char* proccess_get_mem_state (pid_t pid);
char* proccess_get_time      (pid_t pid);

#endif /* __PROCCESS_H__ */
