

/* do you want to hack SEARCH_CASE_INSENSITIVE on GtkTextBuffer? :) */

static const char *forbidden_words[] = {
	"password",
	"Password",
	"PASSWORD",
	"username",
	"Username",
	"USERNAME",
	"http://",
	"visa",
	"Visa",
	"VISA",
	"mastercard",
	"Mastercard",
	"MASTERCARD",
	"bank",
	"Bank",
	"BANK",
	"banking",
	"Banking",
	"BANKING",
	"account",
	"Account",
	"ACCOUNT",
	"secret",
	"Secret",
	"SECRET",
	"login",
	"Login",
	"LOGIN",
	"account",
	"Account",
	"ACCOUNT",
	NULL
};
