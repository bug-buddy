/* bug-buddy bug submitting program
 *
 * Copyright (C) 2001 Jacob Berkman
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  jacob berkman  <jacob@bug-buddy.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __BUGZILLA_H__
#define __BUGZILLA_H__

#include <libxml/parser.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>

#include <libsoup/soup-message.h>

extern GQuark bugzilla_error_quark (void);
#define BUGZILLA_ERROR bugzilla_error_quark ()

typedef enum {
	BUGZILLA_ERROR_RECV_BAD_STATUS,
	BUGZILLA_ERROR_RECV_PARSE_FAILED,
	BUGZILLA_ERROR_SEND_NOTSUPPORTED_APP,
	BUGZILLA_ERROR_SEND_ERROR
} BugzillaError;

enum {
        PRODUCT_ICON,
        PRODUCT_NAME,
        PRODUCT_DESC,
        PRODUCT_DATA,

        PRODUCT_COLS
};


typedef struct {
	char *name;
	char *cname;
	char *comment;
	char *icon;
	char *bugzilla;
	char *product;
	char *component;
	char *version;
	char *extra_info_script;
	int   ref_count; 
} BugzillaApplication;

struct _GnomeVersionInfo {
        char *gnome_platform;
        char *gnome_distributor;
        char *gnome_date;
};

typedef struct _GnomeVersionInfo GnomeVersionInfo;
	

GHashTable*        load_applications       (void);
char*              bugzilla_parse_response (SoupMessage *msg,
                                            GError **err);
gboolean	   bugzilla_search_for_package (gpointer key, gpointer value, const char *package);

SoupMessage*       bugzilla_create_report  (BugzillaApplication *app, 
					    int type,
                                            GnomeVersionInfo *gnome_version,
                                            const char *username,
                                            const char *title,
                                            const char *text,
                                            GtkBuilder *ui,
                                            GError **err);

#endif /* __BUGZILLA_H__ */
